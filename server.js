const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
module.exports = function() {
	return "Hello world";
}

const app = express();

mongoose.connect('mongodb://localhost:27017/products',{useNewUrlParser: true, useUnifiedTopology: true})
	.catch(error => {console.error('Connection failed', error.message)});

const {Schema} = mongoose;
const productSchema = new Schema({
  name: String,
  description: String,
})
var Product = mongoose.model('products', productSchema);

app.get('/api/product', async (req, res) => {
  let products = await Product.find();
  return res.status(200).send(products);
});

app.use(
  bodyParser.urlencoded({
    extended: true
  })
)

app.use(bodyParser.json())

app.post('/api/product', async (req, res) => {
  console.log("POST "+req.body.name+", "+req.body.description);
  let product = await Product.create(req.body);
  return res.status(201).send({error: false,product})
})

app.listen(5000, () => {
  console.log('Server running on port 5000')
});


/*
 *  This version supports a POST request.  The following is added -
 *
 *		const bodyParser = require('body-parser');
 *	
 *	body-parser, as its name suggest will parse the JSON in the body the request for convenient
 *	JavaScript use.
 *
 *	Then the following are added:
 *
 *		app.use(
 *		  bodyParser.urlencoded({
 *		    extended: true
 *		  })
 *		)
 *		
 *		app.use(bodyParser.json())
 *
 *	Having required the body-parser, these lines configure the body-parser and engage its use in app.
 *  It means the parser will accept only UTF-8 encoding of the body and supports automatic inflation 
 *	of gzip and will deflate encodings. A new body object containing the parsed data is populated on 
 *	the request object.  This object will contain key-value pairs, where the value can be a string or 
 *	array (when extended is false), or any type (when extended is true).
 *
 *		      mongoimport --db products --collection products --file products.json
 * 
 * nicnei@gmail.com
 */